Feature: Holiday Planner
 
Scenario: A happy holidaymaker
    Given I like to holiday in
    |   City    |
    |   Sydney  |
    And I only like to holiday on
    |   Day         |
    |   Thursday    |
    When I look up the weather forecast
    Then I receive the weather forecast
    And the temperature is warmer than 
    |   Min_temp    |
    |   10          |