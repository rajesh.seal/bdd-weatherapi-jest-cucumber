module.exports = {
    verbose: true,
    testMatch: [
        "**/*.steps.js"
      ],
      reporters: [
        "default",
        ["./node_modules/jest-html-reporter", {
            "pageTitle": "Test Report",
            "outputPath": "test-report/report.html",
            "includeFailureMsg": true,
            "includeConsoleLog": true
        }]
    ],

  };