import { defineFeature, loadFeature } from 'jest-cucumber';
const feature = loadFeature('./features/holidayPlanner.feature');
const getWeather = require('../operationObjects/getWeather.js');

defineFeature(feature, test => {
  test('A happy holidaymaker', ({ given, when, then }) => {
    given('I like to holiday in', table => {
          getWeather.setData ('favCity',table[0].City)
    });
    given('I only like to holiday on', table => {
          getWeather.setData ('favDay',table[0].Day)
    });
    when('I look up the weather forecast', () => {
          return getWeather.checkForecast().then (data =>{
          getWeather.setGETResponse(data)
      });      
    });
    then('I receive the weather forecast', () => {
          getWeather.chkResponseReceived()
    });
    then('the temperature is warmer than', table => {
          getWeather.confirmPlan(table[0].Min_temp)
    });
  });
});