const fetch  = require('node-fetch');
const assert = require('assert');
const d = require('../utils/dayConversion.js');
const v = require('../utils/varList.js');

//set favorite day and city
function setData(vKey, vData){
    if (vKey == 'favCity') {
        v.qCity=vData;  
    }
    if (vKey == 'favDay') {
        v.qDay=vData;
    }
    v.queryString= `q=${v.qCity}&mode=${v.qMode}&units=${v.qUnits}&appid=${v.qAppid}`;
}

// query weather service for day's forecast
  function checkForecast(){
    v.url= v.pathURL + v.queryString;
    return  fetch(v.url)
            .then(checkStatus)  //call helper function
            .then((response) => {
                return response.json()
            }).then((data) => {
                return data;
            })
}

//helper function to check status and content type
function checkStatus(res) {
    if (res.status == 200) { // res.status >= 200 && res.status < 300
        assert.strictEqual(res.headers.get('content-type'), 'application/json; charset=utf-8');
        v.flagGetResonse= true;
        return res;
    } else {
        v.flagGetResonse= false;
        throw "No response recieved from API"  // Need to enhance the error handling 
    }
}

//helper function to persist the json response
function setGETResponse(obj){
        v.resObj= obj
}

// check if response recieved flag is true
function chkResponseReceived(){
    if (v.flagGetResonse!= true) {
        throw "I have not recieved weather forecast "  // Need to enhance the error handling
    }
}

//check if city, day and temperature is apt
function confirmPlan(minTemp) {
    if (d.dayName() !=v.qDay) {
        throw "Not your favorite day for holiday!"  // Need to enhance the error handling
    }
    if (v.resObj.main.temp_min < minTemp) {
        throw "Its too cold for a holiday today"  // Need to enhance the error handling
    }
}

module.exports = {
setData: setData,
checkForecast: checkForecast,
setGETResponse: setGETResponse,
chkResponseReceived: chkResponseReceived,
confirmPlan:confirmPlan
}