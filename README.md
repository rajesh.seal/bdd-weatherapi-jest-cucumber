# bdd-weatherapi-jest-cucumber
---
About:
The project is to demonstrate processing of API calls to get the weather info. The feature is about a user fetching weather info and deciding holiday based on temperature and day of week.
It is data driven and can be configured in feature file.

---
##To run test on local machine
Clone repo on your system. Please make sure you have `NodeJS` installed on your system.

1. Run `npm install` to install dependencies on system.
2. Run `npm test` on seperate terminal to run tests.
 
---
##Note about config:
1. Reports: The configs for report are available at `jest.config.js` under `reporters` tag. Default location of report is `test-report` folder
2. The input and expected user data can be configured in `holidayPlanner.feature`
---
##Asssumtions:
1. The 3rd party API is in `online` and `serving` requests.

---
##Enhancements:
1. Utils need to be enriched for common calls for CRUD operations by decoupling them from tests.
2. Error handling.
3. Configuring the project for Gitlab CI.
---